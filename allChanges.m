function [ rows ] = allChanges( nf )
%allChanges All the small legal petabutions of a policy.
%   Detailed explanation goes here
S=repmat(nf,1,length(nf));

SS=[(S+eye(length(nf),'uint16')) (S-eye(length(nf),'uint16'))];



SS(1,:)=1;
SS(size(SS,1),:)=max(nf);
SS=max(min(SS,max(nf)),1);


boolArr=arrayfun(@(c) checkNonDecreasing(SS(:,c)),1:size(SS,2));

rows=unique((SS(:,boolArr))','rows')';

end


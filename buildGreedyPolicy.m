function [ policy ] = buildGreedyPolicy( varargin )
%buildGreedyPolicy This function builds the buyMatrix from the parameters


global epsilonSA
global mexFlag

parameters=varargin{1};

if nargin==1
    constrained=-1;
else
    constrained=varargin{2};
end

display('starting fTable');
if mexFlag
    fTable=getFTableMex(parameters.costTable,parameters.mu,parameters.lambda,parameters.lambda_r,epsilonSA,constrained);
else
    fTable=getFTable(parameters,constrained);
end
display('finisged fTable');

[r c]=size(fTable);

policy=cell2mat(arrayfun(@(i) find(fTable(i,1:c)==parameters.costTable(i,1:c),1,'first'),1:r,'UniformOutput',false));

policy=min(policy, parameters.maxQueue);

I=find(policy==max(policy),1,'first');

policy=uint16(policy(1:I))';

end


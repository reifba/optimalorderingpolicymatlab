function [ nf ] = buildBaseStockPolicy(maxQueue,baseStock)    
    nf=[ones(baseStock+1,1,'uint16'); ones(maxQueue-baseStock-1,1,'uint16')*maxQueue];
end


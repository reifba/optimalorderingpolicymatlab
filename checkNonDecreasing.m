function [ bool ] = checkNonDecreasing( nf )
%checkMonotonicity Check if n_f is monotone:
 bool=all(nf(2:length(nf))>=nf(1:length(nf)-1));   

end


#include "mex.h"
#include "matrix.h"

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
    double *stationaryMatrix,*lambdaPt,*lambda_rPt,*orm;
    double rate,lambda,lambda_r;
    mxLogical  *policyMatrix;
    mwSize r,c;
    int i,j,a;
    
    
    
    policyMatrix=mxGetLogicals(prhs[0]);
    stationaryMatrix=mxGetPr(prhs[1]);
    r=mxGetM(prhs[0]);
    c=mxGetN(prhs[0]);
    
    lambdaPt=mxGetData(prhs[2]);
    lambda=lambdaPt[0];
    
    lambda_rPt=mxGetData(prhs[3]);
    lambda_r=lambda_rPt[0];
    
    
    plhs[0] = mxCreateNumericMatrix(r, c, mxDOUBLE_CLASS, mxREAL);
    orm =  mxGetPr(plhs[0]);
    
    
    for(i=1;i<r;i++){
        for(j=0;j<c;j++){
            if(!policyMatrix[j*r+i]){
                if(policyMatrix[j*r+i-1]){
                    rate=stationaryMatrix[i+j*r]*lambda;
                    a=0;
                    while(policyMatrix[i-1+a+(j+a)*r]){
                        orm[i-1+a+(j+a)*r]+=rate;
                        a++;
                    }
                    
                }
                if(policyMatrix[(j+1)*r+i]){
                    rate=stationaryMatrix[i+j*r]*lambda_r;
                    a=0;
                    while(policyMatrix[i+a+(j+a+1)*r]){
                        orm[i+a+(j+a+1)*r]+=rate;
                        a++;
                    }
                }
            }
        }
    }
    
}

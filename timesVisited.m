function [ orm ] = timesVisited( policyMatrix,stationaryMatrix,parameters)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
[r,c]=size(policyMatrix);
orm=zeros(r,c);


indeces=find(~policyMatrix);

for index=1:numel(indeces);
    [i,j] = ind2sub([r,c],index);
    %lambda
    if policyMatrix(i-1,j)
        rate=stationaryMatrix(i,j)*parameters.lambda;
        a=0;
        while policyMatrix(i-1+a,j+a)
            orm(i-1+a,j+a)=orm(i-1+a,j+a)+rate;
            a=a+1;
        end
    end
    
    %lambda_R
    if policyMatrix(i,j+1)
        rate=stationaryMatrix(i,j)*parameters.lambda_r;
        a=0;
        while policyMatrix(i+a,j+1+a)
            orm(i+a,j+a+1)=orm(i+a,j+a+1)+rate;
            a=a+1;
        end
    end
end


% for i=2:r
%     for j=1:(c-1)
%         if ~policyMatrix(i,j)
%             %lambda
%             if policyMatrix(i-1,j)
%                 rate=stationaryMatrix(i,j)*parameters.lambda;
%                 a=0;
%                 while policyMatrix(i-1+a,j+a)
%                     orm(i-1+a,j+a)=orm(i-1+a,j+a)+rate;
%                     a=a+1;
%                 end
%             end
%             
%             %lambda_R
%             if policyMatrix(i,j+1)
%                 rate=stationaryMatrix(i,j)*parameters.lambda_r;
%                 a=0;
%                 while policyMatrix(i+a,j+1+a)
%                     orm(i+a,j+a+1)=orm(i+a,j+a+1)+rate;
%                     a=a+1;
%                 end
%             end
%         end
%         
%     end
% end



end


#include "mex.h"
#include "matrix.h"


void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
    double *muPt,*lambdaPt,*previousFTable,*lambda_rPt,*f,*costTable,*epsilonPt,*constrainPt;
    double mu,lambda,lambda_r,epsilon,err,constrain,temp;
    int N,IP,i,j,size,k;
    mwSize r,c;
    
    costTable=mxGetData(prhs[0]);
    
    
    r=mxGetM(prhs[0]);
    c=mxGetN(prhs[0]);
    
    
    
    previousFTable=mxCalloc(r*c, sizeof(double));
    f=mxCalloc(r*c, sizeof(double));
    
    muPt=mxGetData(prhs[1]);
    mu=muPt[0];
    
    lambdaPt=mxGetData(prhs[2]);
    lambda=lambdaPt[0];
    
    lambda_rPt=mxGetData(prhs[3]);
    lambda_r=lambda_rPt[0];
    
    epsilonPt=mxGetData(prhs[4]);
    epsilon=epsilonPt[0];
    
    constrainPt=mxGetData(prhs[5]);
    constrain=constrainPt[0];
    
    /*copy cost in f*/
    for(k=0;k<r*c;k++){
        f[k]=costTable[k];
    }
    
    
    for(i=constrain+2;i<r;i++){
       
        
        for(j=0;j<c;j++){
            f[j*r+i]=f[j*r+(i-1)];
        }
        err=1024.0;
        while(err>epsilon){
           
            for(k=0;k<r*c;k++){
                previousFTable[k]=f[k];
            }           
           
            
            f[i]=(lambda*previousFTable[i-1]+lambda_r*previousFTable[r+i])/(lambda+lambda_r);
            f[(c-1)*r+i]=previousFTable[(c-2)*r+i];
            for(j=1;j<c-1;j++){
                f[j*r+i]=(lambda*previousFTable[j*r+i-1]+lambda_r*previousFTable[(j+1)*r+i]+mu*previousFTable[(j-1)*r+i])/(lambda+lambda_r+mu);
            }
            
            for(j=0;j<c;j++){
                if(costTable[j*r+i]<f[j*r+i]){
                    f[j*r+i]=costTable[j*r+i];
                }
            }
            
            err=0.0;
            for(k=0;k<r*c;k++){
                temp=previousFTable[k]-f[k];
                if(temp<0)
                    temp=-temp;
                
                err+=temp;
            }
        }

    }
	
	
	/*
    err=1024.0;
    while(err>epsilon){
        mxFree(previousFTable);
        previousFTable=f;
        f=mxCalloc(r*c, sizeof(double));
        for(i=0;i<r;i++){
            for(j=0;j<c;j++){
                N=j;
                IP=i-1;
                if(IP<=constrain)
                    f[j*r+i]=costTable[j*r+i];
                else if(N==0)
                    f[j*r+i]=(lambda*previousFTable[j*r+i-1]+lambda_r*previousFTable[(j+1)*r+i])/(lambda+lambda_r);
                else if(N==c-1)
                    f[j*r+i]=previousFTable[(j-1)*r+i];
                else
                    f[j*r+i]=(lambda*previousFTable[j*r+i-1]+lambda_r*previousFTable[(j+1)*r+i]+mu*previousFTable[(j-1)*r+i])/(lambda+lambda_r+mu);
                
                
                if(costTable[j*r+i]<f[j*r+i])
                    f[j*r+i]=costTable[j*r+i]; 
                
            }
        }
        
        err=0;
        for(k=0;k<r*c;k++){
            temp=previousFTable[k]-f[k];
            if(temp<0)
                temp=-temp;
            
            err+=temp;
        }
        
        
    }
    */
    /*mxFree(previousFTable);*/
    
    plhs[0] = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
    mxSetData(plhs[0], f);
    mxSetM(plhs[0], r);
    mxSetN(plhs[0], c);
    
}

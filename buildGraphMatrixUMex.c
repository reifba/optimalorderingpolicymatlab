#include "mex.h"
#include "matrix.h"


void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
    int a,sourceIndex,destIndex,i,j,count;
    double *muPt,*lambdaPt,*lambda_rPt,*rows,*columns, *values;
    double mu,lambda,lambda_r,total,muPr,lambdaPr,lambda_rPr;
    mwSize r,c;
    mxLogical  *policyMatrix;
    
    
    
    
    if (nrhs != 4)
        mexErrMsgTxt("The number of input arguments must be 4.");
    
    
    r=mxGetM(prhs[0]);
    c=mxGetN(prhs[0]);
    
    
    policyMatrix=mxGetLogicals(prhs[0]);
    
    muPt=mxGetData(prhs[1]);
    mu=muPt[0];
    
    lambdaPt=mxGetData(prhs[2]);
    lambda=lambdaPt[0]; 
    
    lambda_rPt=mxGetData(prhs[3]);
    lambda_r=lambda_rPt[0];
    
    total=mu+lambda+lambda_r;
    
	muPr       = mu/total;
	lambdaPr   = lambda/total;
	lambda_rPr = lambda_r/total;
    
    plhs[0] = mxCreateNumericMatrix(r*c*3,1, mxDOUBLE_CLASS, mxREAL);
    plhs[1] = mxCreateNumericMatrix(r*c*3,1, mxDOUBLE_CLASS, mxREAL);
    plhs[2] = mxCreateNumericMatrix(r*c*3,1, mxDOUBLE_CLASS, mxREAL);
    
    
    
    
    rows    =  mxGetPr(plhs[0]);
    columns =  mxGetPr(plhs[1]);
    values  =  mxGetPr(plhs[2]);
    
    
    count =0;
    
    for( i=0;i<r;i++){
        for( j=0;j<c;j++){
            if(!policyMatrix[j*r+i]){
                sourceIndex=j*r+i;
                if (j>0){
                    for(a=0;j+a<c && policyMatrix[i+a+(j+a-1)*r];a++);
                    destIndex=(j+a-1)*r+(i+a);
                }
                else{
                    destIndex=sourceIndex;
                }
                rows[count]=sourceIndex;
                columns[count]=destIndex;
                values[count]=muPr;
                
                count++;
                
                
                if (j<c-1 && i>0){
                    for(a=0;policyMatrix[(j+a)*r+i+a-1];a++);
                    destIndex=(j+a)*r+i+a-1;
                }
                else{
                    destIndex=sourceIndex;
                }
                rows[count]=sourceIndex;
                columns[count]=destIndex;
                values[count]=lambdaPr;
                
                count++;
                
                
                if (j<c-1){
                    for( a=0;policyMatrix[(j+a+1)*r+i+a];a++);
                    destIndex=(j+a+1)*r+i+a;
                }
                else{
                    destIndex=sourceIndex;
                }
                rows[count]=sourceIndex;
                columns[count]=destIndex;
                values[count]=lambda_rPr;
                
                count++;
            }
        }
    }
	
	while(count<r*c*3){
		rows[count]=0.0;
        columns[count]=0.0;
        values[count]=0.0;
		count++;
	}
}

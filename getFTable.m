function [ fTable ] = getFTable(parameters,constrained)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

global epsilonSA

mustbuy=constrained+2;

[r c]=size(parameters.costTable);

total=parameters.mu+parameters.lambda+parameters.lambda_r;

noEos=parameters.lambda+parameters.lambda_r;

lambda=parameters.lambda;
lambda_r=parameters.lambda_r;
mu=parameters.mu;


fTable=parameters.costTable;


%fTable(1,:)=parameters.costTable(1,:);

err=1024;

rowIndex=(mustbuy+1):r;
columnIndex=2:(c-1);
%fTable(rowIndex,columnIndex)=min(min(parameters.costTable));


while err> epsilonSA
    
    previousFTable=fTable;
    
    %i<=mustbuy never changes
    
    %first column j==1
  
    fTable(rowIndex,1)= (lambda*previousFTable(rowIndex-1,1)+lambda_r*previousFTable(rowIndex, 2))./ noEos;
    
    %all but last Queue j~=size(fTable,2)
   
    fTable(rowIndex,columnIndex)=( mu .* previousFTable(rowIndex, columnIndex-1) + lambda_r .* previousFTable(rowIndex, columnIndex + 1) +lambda .* previousFTable(rowIndex-1, columnIndex)) ./ total  ;
    
    %j==size(fTable,2)
    fTable(2:end,end)=previousFTable(2:end,end-1);
    
    fTable=min(parameters.costTable,fTable);
    
    err=sum(sum(abs(previousFTable-fTable)));
    display(err);
end


end


function [ costTable ] = buildCostTable( size,mu,lambda,h,p )
costTable=zeros(size,size);

for i=1:size
    for j=1:size
        
        IP=i-2;
        N=j-1;
        
        if IP==-1
            costTable(i,j)=p*(N+1)/mu;
        elseif N==0
            costTable(i,j)=(lambda/(lambda+mu))*costTable(i-1,j)+(mu/(lambda+mu))*(IP+1)*h/lambda;
        else
            costTable(i,j)=(lambda/(lambda+mu))*costTable(i-1,j)+(mu/(lambda+mu))*costTable(i,j-1);
        end
        
    end
end


end



%a=[0.38 0.25 0.4 0.5; 0.52 0.4 0.52 0.63; 0.65 0.6 0.63 0.74];
function prob=dp(engLeft,phase,a)
    if phase==0
        prob=1;
    else
        temp=zeros(1,3);
        for i=0:min(2,engLeft)
            temp(i+1)=a(i+1,phase)*dp(engLeft-i,phase-1,a);
        end       
        [ prob T]=max(temp);
        display([engLeft,phase, T-1]);
    end
end



function [ policyMatrix ] = nFtoPolicyMatrix( n_f )

r=length(n_f);
c=max(n_f);
policyMatrix=false(r,c);

for i = 1:r
    policyMatrix(i,n_f(i):c)=true;
end


policyMatrix(1,:)=true;
policyMatrix(r,:)=false;
policyMatrix(:,c)=false;


end
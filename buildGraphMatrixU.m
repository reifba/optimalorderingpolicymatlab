function [ graphMatrix ] = buildGraphMatrixU(policyMatrix,parameters  )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[r,c]=size(policyMatrix);


rows=zeros(r*c*3,1);
columns=zeros(r*c*3,1);
values=zeros(r*c*3,1);

total=parameters.mu+parameters.lambda+parameters.lambda_r;

count=1;

for i=1:r
    for j=1:c
        
        sourceIndex=(j-1)*r+i;
        
        %non real situations
        
        if policyMatrix(i,j)
            
            destIndex=j*r+i+1;
            
            rows(count)=sourceIndex;
            columns(count)=destIndex;
            values(count)=1;
            
            count=count+1;
            
        else
            %mu
            if j>1
                a=0;
                
                while policyMatrix(i+a,j+a-1)
                    a=a+1;
                end
                
                destIndex=(j+a-2)*r+i+a;
                
                rows(count)=sourceIndex;
                columns(count)=destIndex;
                values(count)=parameters.mu/total;
                
                count=count+1;
            else
                rows(count)=sourceIndex;
                columns(count)=sourceIndex;
                values(count)=parameters.mu/total;
                
                count=count+1;
            end
            
            %lambda
            if j<parameters.maxQueue && i>1
                
                a=0;
                
                while policyMatrix(i+a-1,j+a)
                    a=a+1;
                end
                
                destIndex=(j+a-1)*r+i+a-1;
                
                rows(count)=sourceIndex;
                columns(count)=destIndex;
                values(count)=parameters.lambda/total;
                
                count=count+1;
                
            else
                
                rows(count)=sourceIndex;
                columns(count)=sourceIndex;
                values(count)=parameters.lambda/total;
                
                count=count+1;                
            end
            
            %lambda_r
            if j<parameters.maxQueue
                
                a=0;
                
                while policyMatrix(i+a,j+a+1)
                    a=a+1;
                end
                
                destIndex=(j+a)*r+i+a;
                
                rows(count)=sourceIndex;
                columns(count)=destIndex;
                values(count)=parameters.lambda_r/total;
                
                count=count+1;
            else
                
                rows(count)=sourceIndex;
                columns(count)=sourceIndex;
                values(count)=parameters.lambda_r/total;
                
                count=count+1;
                
            end
            
            
        end
    end
    
    graphMatrix=sparse(rows(1:count-1),columns(1:count-1),values(1:count-1));
    
   
    
end


function []=Main()



 
global epsilonQueue
global epsilonSA
global epsilonPolicy
global mexFlag

mexFlag=true;

if mexFlag
    mex buildGraphMatrixMex.c
    mex buildGraphMatrixUMex.c
    mex timesVisitedMex.c
    mex getFTableMex.c
    mex buildCostTableMex.c
end




epsilonQueue=10^-6;
epsilonSA=10^-14;
epsilonPolicy=10^-14;




parameters={};

  val=0.3:0.1:0.8;
 res=zeros(1,length(val));



%load('res.mat');

for j=1:length(val)
    parameters{j}=createParameters(0.5,val(j),5/6);
end

for i=1:length(val)
    if res(i)==0
        [greedy2 vg]=findBestGreedy(parameters{i},false);
        display(greedy2);
        [opt1 v]=findOptimaPolicy(parameters{i},greedy2,false);
        res(i)=vg/v;
        save('res.mat', 'res','val');
    end
end


% 
% 
% parameters=createParameters(0.5,0.5,0.8);
% 
% 
% %greedy1=buildGreedyPolicy(parameters);
% 
% 
% [greedy2 vg]=findBestGreedy(parameters,false);
% [opt1 v]=findOptimaPolicy(parameters,greedy2,false);
% 
% display(vg/v);
% 
% %m=min(length(greedy1),length(opt1));
% m=min(length(greedy1),length(greedy2));
% 
% t=[greedy2(1:m)-1 opt1(1:m)-1 ];
% figure
% bar(t)


% HeatMap(-log(1-cp_greedy1.timeProportionMatrix'))
% HeatMap(-log(1-cp_greedy2.timeProportionMatrix'))
% HeatMap(-log(1-cp_opt1.timeProportionMatrix'))

end









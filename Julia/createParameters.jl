type Parameters
	mu
	lambda
	lambda_r
	h
	p
	rho
	c
	beta
	maxQueue
	costTable
end


function createParameters(rho,c,beta)
	mu=100.0;
    h=100.0;
	lambda=c*rho*mu;
    lambda_r=(1-c)*rho*mu;
    p=h*beta/(1-beta);

	max_maxQueue=1;
    while rho^maxQueue>epsilonQueue
		maxQueue++;
    end
        maxQueue=round(2*max_queue+1);
	
	ct=zeros(500,500);
	
	customerArrivesProbability = lambda / (lambda + mu);
	
	endOfServiceProbability= mu / (lambda + mu);
	
    for i=1:size(ct,1)
		for j=1:size(ct,2)
			if i==1
				ct[i,j] = j * p / mu;
            else
                if j==1
					eos=(i-1) * h / lambda;
				else
                  	eos=ct[i,j-1];
                end		
			ct[i,j]=customerArrivesProbability * ct[i-1,j] + endOfServiceProbability * eos;
			end
		end
    end

	return Parameters(mu,lambda,lambda_r,h,p,rho,c,beta,maxQueue,ct)
end

function createParameters(mu,lambda,lambda_r,h,p)
	rho=(lambda+lambda_r)/mu
	c=(lambda)/(lambda+lambda_r)
	beta=p/(h+p)

	max_maxQueue=1;
    while rho^maxQueue>epsilonQueue
		maxQueue++;
    end
        maxQueue=round(2*max_queue+1);
	
	ct=zeros(500,500);
	
	customerArrivesProbability = lambda / (lambda + mu);
	
	endOfServiceProbability= mu / (lambda + mu);
	
    for i=1:size(ct,1)
		for j=1:size(ct,2)
			if i==1
				ct[i,j] = j * p / mu;
            else
                if j==1
					eos=(i-1) * h / lambda;
				else
                  	eos=ct[i,j-1];
                end		
			ct[i,j]=customerArrivesProbability * ct[i-1,j] + endOfServiceProbability * eos;
			end
		end
    end

	return Parameters(mu,lambda,lambda_r,h,p,rho,c,beta,maxQueue,ct)
end
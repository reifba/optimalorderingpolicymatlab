function  = checkNonDecreasing( nf )
	%checkMonotonicity Check if n_f is monotone:
 	return all(nf(2:length(nf))>=nf(1:length(nf)-1))
end


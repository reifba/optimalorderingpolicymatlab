function nFtoPolicyMatrix( nf )

	r=length(nf)
	c=max(nf)
	policyMatrix=false(r,c)

	for i = 1:r
    		policyMatrix[i,nf(i):c]=true
	end


	policyMatrix[1,:]=true;
	policyMatrix[r,:]=false;
	policyMatrix[:,c]=false;

	return policyMatrix
end


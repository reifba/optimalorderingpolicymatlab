function getFTable(parameters,mustbuy)
	mu=parameters.mu
	lambda=parameters.lambda
	lambda_r=parameters.lambda_r
	maxQueue=parameters.maxQueue	
	
	total=mu+lambda+lambda_r;

	noEos=lambda+lambda_r;

	extra=5;

	minIp = first(find ( min(parameters.costTable[:,1])== parameters.costTable[:,1]));

	fTable=zeros(minIp+maxQueue+extra,maxQueue+extra);
	
	fTable=parameters.costTable[1:size(fTable,1),1:size(fTable,2)];

	rowIndex=(mustbuy+1):size(fTable,1);
	columnIndex=2:(size(fTable,2)-1));

	err=1024

	while err > epsilonSA
    
    	previousFTable=fTable;
    
    	#i<=mustbuy never changes
    
    	#first column j==1
  
    	fTable[rowIndex,1]= (lambda*previousFTable[rowIndex-1,1] + lambda_r*previousFTable[rowIndex, 2])./ noEos;
    
		#all but last Queue j~=size(fTable,2)
   
		fTable[rowIndex,columnIndex]=( mu*previousFTable[rowIndex, columnIndex-1] + lambda_r* previousFTable([rowIndex, columnIndex + 1] +lambda*previousFTable[rowIndex-1, columnIndex]) ./ total  ;
    
		#j==size(fTable,2)
		fTable[:,size(fTable,2)]=previousFTable[:,size(fTable,2)-1];
    
		fTable=min(parameters.costTable[1:size(fTable,1),1:size(fTable,2)],fTable);
    
		err=sum(sum(abs(previousFTable-fTable)));
    
end


fTable=fTable[1:(minIp+parameters.maxQueue),1:parameters.maxQueue];

return fTable
	
end


function getFTable(parameters)
	return getFTable(parameters,1);
end

function  buildGreedyPolicy( parameters )
	#buildGreedyPolicy This function builds the nf from the parameters
	
	fTable=getFTable(parameters);
	(r, c)=size(fTable);
	policy=ones(1,r)*parameters.maxQueue

	for i=1:r
		temp= fTable[i,1:c]==parameters.costTable(i,1:c)
		if any(temp)
			policy(i)=first(temp)
		else
			break
		end
	end

	return policy
end


function  buildGreedyPolicy( parameters,mustbuy )
	#buildGreedyPolicy This function builds the nf from the parameters
	
	fTable=getFTable(parameters,mustbuy);
	(r, c)=size(fTable);
	policy=ones(1,r)*parameters.maxQueue

	for i=1:r
		temp= fTable[i,1:c]==parameters.costTable(i,1:c)
		if any(temp)
			policy(i)=first(temp)
		else
			break
		end
	end

	return policy
end



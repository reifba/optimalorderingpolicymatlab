#include "mex.h"
#include "matrix.h"


void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]) {
    double *muPt,*lambdaPt,*hPt,*pPt,*sizePt,*costTable;
    double mu,lambda,customerArrivesProbability,endOfServiceProbability,h,p,eos;
	int N,IP;
	int i,j,size;
	
	
	sizePt=mxGetData(prhs[0]);
	size=(int)sizePt[0];
    
    muPt=mxGetData(prhs[1]);
    mu=muPt[0];
    
    lambdaPt=mxGetData(prhs[2]);
    lambda=lambdaPt[0];
    
    hPt=mxGetData(prhs[3]);
    h=hPt[0];
	
	pPt=mxGetData(prhs[4]);
    p=pPt[0];
	
	
	customerArrivesProbability = lambda / (lambda + mu);
	endOfServiceProbability= mu / (lambda + mu);	
	
	plhs[0] = mxCreateNumericMatrix(size, size, mxDOUBLE_CLASS, mxREAL);
	
	costTable=  mxGetPr(plhs[0]);
	
	
	for( i=0;i<size;i++){
        for( j=0;j<size;j++){
			N=j;
			IP=i-1;
			if(IP==-1)
				costTable[j*size+i] = (N+1) * p / mu;			
			else if(N==0)
				costTable[j*size+i]=customerArrivesProbability*costTable[(i-1)+j*size]+endOfServiceProbability*(IP+1)*h/ lambda;			
			else
				costTable[j*size+i]=customerArrivesProbability * costTable[(i-1)+j*size] +endOfServiceProbability *costTable[i+(j-1)*size];			
		}
	}
	 
	
}
function [ params ] = createParameters(varargin)
    global epsilonQueue
    global mexFlag
    switch nargin
        case 3
            rho=varargin{1};
            c=varargin{2};
            beta=varargin{3};
            mu=100.0;
            h=100.0;
            lambda=c*rho*mu;
            lambda_r=(1-c)*rho*mu;
            p=h*beta/(1-beta);            
        case 5
            mu=varargin{1};
            lambda=varargin{2};
            lambda_r=varargin{3};
            h=varargin{4};
            p=varargin{5};
            rho=(lambda+lambda_r)/mu;
            c=lambda/(lambda+lambda_r);
            beta=h/(h+p);       
    end
    
    
    %compute max queue
    max_queue=round(2*log(epsilonQueue)./log(rho)+1);   
    
    %compute Cost Table:
    if mexFlag
        ct=buildCostTableMex(800,mu,lambda,h,p);     
    else
        ct=buildCostTable(800,mu,lambda,h,p);
    end
    
        
	params = struct('mu', mu,'lambda',lambda,'lambda_r',lambda_r,'h',h,'p',p,'rho',rho,'c',c,'beta',beta,'maxQueue',max_queue,'costTable',ct);
end


function [ expectedCost ] = getPolicyExpectedCost( varargin )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

global mexFlag

if nargin <=1 || nargin>3
    exception = MException('exp:nargin', 'wrong number of arguments');
    throw(exception);
end

nf=varargin{1};
parameters=varargin{2};


I=find(nf==max(nf),1,'first');

nf_turncated=nf(1:I);

policyMatrix=nFtoPolicyMatrix(nf_turncated);

[r,c]=size(policyMatrix);

linesWithZeros=~logical(reshape(policyMatrix,numel(policyMatrix),1));

if mexFlag
    M=buildGraphMatrixMexWrapper(policyMatrix,parameters);
else
    M=buildGraphMatrix(policyMatrix,parameters);
end

S=buildRateMatix( M );

S2=S(linesWithZeros,linesWithZeros);

u2=sparse(length(S2),1,1);


pVecTemp=full(S2\u2);



pVecFull=zeros(length(numel(policyMatrix)),1);
pVecFull(linesWithZeros)=pVecTemp;


tpm=reshape(pVecFull,r,c);


if mexFlag
    orm = timesVisitedMex(policyMatrix,tpm,parameters.lambda,parameters.lambda_r);
else
    orm = timesVisited(policyMatrix,tpm,parameters);
end





expectedCost = sum(sum(orm.*parameters.costTable(1:r,1:c)));

end


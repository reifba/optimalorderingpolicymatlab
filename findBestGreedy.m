function [ current_greedy current_cost ] = findBestGreedy( parameters,uniform )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
current_greedy=buildGreedyPolicy(parameters);
display(current_greedy);
current_cost=getPolicyExpectedCost(current_greedy,parameters,uniform);


minIP=find(current_greedy==1,1,'last')-2;

next_greedy=buildGreedyPolicy(parameters,minIP+1);
next_cost=getPolicyExpectedCost(next_greedy,parameters,uniform);

while  next_cost<current_cost
    
    current_cost=next_cost;
    current_greedy=next_greedy;
        
    minIP=find(current_greedy==1,1,'last')-2;
        
    next_greedy=buildGreedyPolicy(parameters,minIP+1);
    next_cost=getPolicyExpectedCost(next_greedy,parameters,uniform);
    
end


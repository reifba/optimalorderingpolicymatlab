function [ graphMatrix ] = buildGraphMatrixMexWrapper  (policyMatrix,parameters)

[R C V]=buildGraphMatrixMex(policyMatrix,parameters.mu,parameters.lambda,parameters.lambda_r);
graphMatrix=sparse(R+1,C+1, V);

end


function [ current_policy,current_cost ] = findOptimaPolicy( varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
global epsilonPolicy

parameters=varargin{1};

if nargin==1
    nf=buildBaseStockPolicy(parameters.maxQueue,10);
    uniform=varargin{2};
else
    nf=varargin{2};
    uniform=varargin{3};
end



current_policy=nf;
current_cost=getPolicyExpectedCost(nf,parameters);

ac=allChanges(current_policy);
vArr=zeros(1,size(ac,2));


parfor x=1:size(ac,2)
    vArr(x)=getPolicyExpectedCost(ac(:,x),parameters,uniform);
end


[temp_cost index]= min(vArr);
temp_policy=ac(:,index);

count=0;

while  temp_cost<current_cost && abs(temp_cost-current_cost)>epsilonPolicy
    
    count=count+1;
    
    display(count)
    display(current_cost-temp_cost)
    
    current_policy=temp_policy;
    current_cost=temp_cost;
    
    
    ac=allChanges(current_policy);
    vArr=zeros(1,size(ac,2));
    parfor x=1:size(ac,2)
        vArr(x)=getPolicyExpectedCost(ac(:,x),parameters,uniform);
    end
    
    
    [temp_cost index]= min(vArr);
    temp_policy=ac(:,index);
    
end



end


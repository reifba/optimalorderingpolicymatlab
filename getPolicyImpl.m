function [ resultStrucut ] = getPolicyImpl( varargin )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%global epsilonQueue

if nargin <=1 || nargin>3
         exception = MException('exp:nargin', 'wrong number of arguments');
     throw(exception);
end

nf=varargin{1};
parameters=varargin{2};    
if nargin==3
    uniformizationFalg=varargin{3};
else
    uniformizationFalg=false;
end



I=find(nf==max(nf),1,'first');

nf_turncated=nf(1:I);

policyMatrix=nFtoPolicyMatrix(nf_turncated);

[r,c]=size(policyMatrix);

linesWithZeros=~logical(reshape(policyMatrix,numel(policyMatrix),1));

if uniformizationFalg
    P=buildGraphMatrixUMexWrapper(policyMatrix,parameters);
    
    Plwz=P(linesWithZeros,linesWithZeros);
    
    pVecTemp = stationary(Plwz);
else
    
    M=buildGraphMatrixMexWrapper(policyMatrix,parameters);
    
    S=buildRateMatix( M );
    
    S2=S(linesWithZeros,linesWithZeros);
    
    u2=sparse(length(S2),1,1);
    
    
    pVecTemp=full(S2\u2);
    
    
end
pVecFull=zeros(length(numel(policyMatrix)),1);
pVecFull(linesWithZeros)=pVecTemp;

tpm=reshape(pVecFull,r,c);

% if abs(sum(tpm(:,1))+parameters.rho-1)>epsilon
%     exception = MException('exp:idleTime', 'the idle time is not 1-rho');
%     throw(exception);
% end


qd=sum(tpm);

ipd=sum(tpm,2);

orm=timesVisitedMex( policyMatrix,tpm,parameters.lambda,parameters.lambda_r);


expectedCost=sum(sum(orm.*parameters.costTable(1:r,1:c)));

resultStrucut=struct('queueDistribution',qd,'ipDistribution',ipd,'orderRateMatrix',orm, 'timeProportionMatrix', tpm, 'cost', expectedCost);

end



global epsilonQueue
global epsilonSA
global epsilonPolicy



epsilonQueue=10^-10;
epsilonSA=10^-16;
epsilonPolicy=10^-16;

mex buildGraphMatrixMex.c
mex buildGraphMatrixUMex.c
mex timesVisitedMex.c
mex getFTableMex.c
mex buildCostTableMex.c


pr1=createParameters(0.7501,0.66002,0.7998);


% greedy=findBestGreedy(pr1,false);
% greedyU=findBestGreedy(pr1,true);
%
% gv=getPolicyExpectedCost(greedy,pr1,false);
% gu=getPolicyExpectedCost(greedyU,pr1,true);
%
% [opt1 v]=findOptimaPolicy(pr1,greedy,false);
% [opt1U vu]=findOptimaPolicy(pr1,greedyU,true);


%
% pr1=createParameters(0.7,0.8,0.8);

nf=buildGreedyPolicy(pr1);

parameters=pr1;

I=find(nf==max(nf),1,'first');

nf_turncated=nf(1:I);

policyMatrix=nFtoPolicyMatrix(nf_turncated);
linesWithZeros=~logical(reshape(policyMatrix,numel(policyMatrix),1));


[r,c]=size(policyMatrix);


M=buildGraphMatrixMexWrapper(policyMatrix,parameters);



Mlwz=M(linesWithZeros,linesWithZeros);
%
%
%
%
%
%
%
%
S=buildRateMatix( M );

%
S2=S(linesWithZeros,linesWithZeros);
S2f=full(S2);

S3=buildRateMatix( Mlwz );
%
% u=sparse(length(S),1,1);
u2=sparse(length(S2),1,1);
pVec1temp=full(S2\u2);


[U s V]=svds(S2,1);


pVec2temp=V*diag(diag(s).^-1)*U'*u2;

%
pVec1Full=zeros(length(S),1);
pVec1Full(linesWithZeros)=pVec1temp;

pVec2Full=zeros(length(S),1);
pVec2Full(linesWithZeros)=pVec2temp;
%
 tpm1=reshape(pVec1Full,r,c);
 tpm2=reshape(pVec2Full,r,c);
%
% % if abs(sum(tpm(:,1))+parameters.rho-1)>epsilonQueue
% %     display([sum(tpm(:,1)),1-parameters.rho ]);
% %     exception = MException('exp:idleTime', 'the idle time is not 1-rho');
% %     throw(exception);
% % end
% %
% P=buildGraphMatrixUMexWrapper(policyMatrix,parameters);
% %
%  Plwz=P(linesWithZeros,linesWithZeros);
% %
%  p4 = stationary(Plwz);
%
%
% pVec4Full=zeros(length(length(S)),1);
% pVec4Full(linesWithZeros)=p4;
%
%
%
% sum(abs(pVec4Full'-pVec2Full))
%
%



parfor i=1:50
    [U s V]=svds(S2,i);
    v(i)=sum(sum(abs(U*s*V'-S2)));
end
[a i]=min(v)    





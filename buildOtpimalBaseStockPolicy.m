function [ current ] = buildOtpimalBaseStockPolicy( parameters )
%buildOtpimalBaseStockPolicy this function finds the optimal base stock policy

baseStock=0;

current=buildBaseStockPolicy(parameters.maxQueue,baseStock);
current_cost=getPolicyImpl(current,parameters).cost;


next=buildBaseStockPolicy(parameters.maxQueue,baseStock+1);
next_cost=getPolicyImpl(next,parameters).cost;

while current_cost>next_cost
    
    baseStock=baseStock+1;
    
    current=next;
    current_cost=next_cost;    
    
    next=buildBaseStockPolicy(parameters.maxQueue,baseStock+1);
    next_cost=getPolicyImpl(next,parameters).cost;
end
end


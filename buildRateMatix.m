function [ rateMatrix ] = buildRateMatix( graphMatrix )
%buildRateMatix: Build the passage rate equations system from the graph
%matrix, it also subsitutes the last row with ones.
%   Detailed explanation goes here
rateMatrix=graphMatrix'-diag(sum(graphMatrix,2));
rateMatrix(length(graphMatrix),:)=ones(1,length(graphMatrix));
end

